<?php
/**
 * @file
 * Template file for field_slideshow
 *
 *
 */
?>
<div id="field_collection_carousel_formatter_field_collection_carousel" class="jcarousel-skin-field_collection_carousel_formatter">
  <div class="jcarousel-container">
    <div class="jcarousel-clip">
      <ul class="jcarousel-list">

        <?php foreach ($elements as $i => $item): ?>
        <li class="jcarousel-item-<?php print $i; ?>">
          <div class="image">
            <a href="<?php print $item['image_data_colorbox_image_style_src']; ?>" class='colorbox'
              title="<?php print $item['caption']; ?>"
              rel="bottom_slideshow">
            <img src="<?php print $item['image_data_carousel_image_style_src']; ?>"
              alt="<?php print $item['image_data']['alt']; ?>"
              title="<?php print $item['image_data']['title']; ?>" />
              </a>
          </div>
          <div class="pre-data">&nbsp;</div>
          <div class="data">
            <div class="caption">
              <?php print $item['caption']; ?>
            </div>
            <div class="teaser">
              <?php print $item['teaser']; ?>
            </div>
            <div class="button">
              <?php print $item['button']; ?>
            </div>
          </div>
        </li>
        <?php endforeach; ?>

      </ul>
    </div>
  </div>
  <div class="jcarousel-next jcarousel-prev-horizontal"><?php print $right_arrow; ?></div>
  <div class="jcarousel-prev jcarousel-next-horizontal"><?php print $left_arrow; ?></div>
</div>

