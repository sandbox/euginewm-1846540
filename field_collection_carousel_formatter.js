/**
 *
 */

(function($){
  Drupal.behaviors.field_collection_carousel_formatter = {
    attach: function(context, settings){

      var carouselops = {
        size: 4,
        scroll: 1,
        animation: 'slow',
        wrap: 'circular',
        auto: 1000
      };
      $('#field_collection_carousel_formatter_field_collection_carousel').jcarousel(carouselops);
    }
  };
})(jQuery);
